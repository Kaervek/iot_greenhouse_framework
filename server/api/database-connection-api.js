const mongoose = require('mongoose');

const URI = 'mongodb://mongodb-server-image:27017/essenswelt';

mongoose.connect(URI)
    .then(db => console.log('INFO: Connected to MongoDB database'))
    .catch(err => console.error(err));
    

module.exports = mongoose;