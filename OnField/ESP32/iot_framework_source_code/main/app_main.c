/* MQTT over Websockets Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"

#include "esp_log.h"

#include "organizer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

/* VARIABLES */
static const char *TAG = "IOT GREENHOUSE ESP32";
static void mainTimerCallback(TimerHandle_t pxTimer);

//static void vTimerCallback(TimerHandle_t pxTimer);

void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %" PRIu32 " bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("mqtt_client", ESP_LOG_VERBOSE);
    esp_log_level_set("mqtt_example", ESP_LOG_VERBOSE);
    esp_log_level_set("transport_base", ESP_LOG_VERBOSE);
    esp_log_level_set("transport_ws", ESP_LOG_VERBOSE);
    esp_log_level_set("transport", ESP_LOG_VERBOSE);
    esp_log_level_set("outbox", ESP_LOG_VERBOSE);

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* TODO LAUNCH INITIALIZATION */
    ESP_ERROR_CHECK(organizerInit());
    /* TODO LAUNCH APP ORGANIZER */
    //ESP_ERROR_CHECK(xTaskCreate(organizerSTM, "organizerSTM", 4048, NULL, 1, NULL));
    uint32_t timerId = 2;
    TimerHandle_t xTimers = xTimerCreate("Timer",               // Just a text name, not used by the kernel.
                                         (pdMS_TO_TICKS(1000)), // The timer period in ticks.
                                         pdTRUE,                // The timers will auto-reload themselves when they expire.
                                         (void *)timerId,       // Assign each timer a unique id equal to its array index.
                                         mainTimerCallback );        // Each timer calls the same callback when it expires.
    if (xTimers == NULL)
                { // The timer was not created.
                }
                else

                {
                    // Start the timer.  No block time is specified, and even if one was
                    // it would be ignored because the scheduler has not yet been
                    // started.
                    if (xTimerStart(xTimers, 0) != pdPASS)
                    {
                        // The timer could not be set into the Active state.
                    }
                }
    
    xTaskCreate(organizerSTM, "organizerSTM", 4048, NULL, 1, NULL);


}

static void mainTimerCallback(TimerHandle_t pxTimer)
{    
    xTaskCreate(organizerSTM, "organizerSTM", 2024, NULL, 1, NULL);
    
}