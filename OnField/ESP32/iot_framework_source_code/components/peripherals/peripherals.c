/* PERIPHERALS COMPONENT

   This component handles the I2C and GPIOs

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "peripherals.h"
#include "esp_err.h"
// #include <stdint.h>
// #include <stddef.h>
// #include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "bme280.h"
#include "freertos/task.h"

/******************************************************************************/
/*!                       Macros                                              */

// int8_t rslt;
//     uint32_t period;
//     struct bme280_dev dev;
//     struct bme280_settings settings;

#define I2C_EXAMPLE_MASTER_SCL_IO           5                /*!< gpio number for I2C master clock */
#define I2C_EXAMPLE_MASTER_SDA_IO           4               /*!< gpio number for I2C master data  */
#define I2C_EXAMPLE_MASTER_NUM              I2C_NUM_0        /*!< I2C port number for master dev */
#define I2C_EXAMPLE_MASTER_TX_BUF_DISABLE   0                /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_RX_BUF_DISABLE   0                /*!< I2C master do not need buffer */


#define WRITE_BIT                           I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT                            I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN                        0x1              /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS                       0x0              /*!< I2C master will not check ack from slave */
#define ACK_VAL                             0x0              /*!< I2C ack value */
#define NACK_VAL                            0x1              /*!< I2C nack value */
#define LAST_NACK_VAL                       0x2              /*!< I2C last_nack value */





int8_t *interfacePointerGLobal;
int8_t * interfacePointerResult;

int8_t rslt;
uint32_t period;

struct bme280_dev dev;
struct bme280_settings settings;
int8_t i2c_master_bme280_read(uint8_t reg_address, uint8_t *data, size_t data_len, void *intf_ptr);
int8_t i2c_master_bme280_write(uint8_t reg_address, const uint8_t *data, size_t data_len, void *intf_ptr);
esp_err_t configuration_bme280();
 
void BME280_delay_msek(uint32_t msek)
{
	vTaskDelay(msek/portTICK_PERIOD_MS);
}

i2c_master_bus_config_t i2c_mst_config = {
    .clk_source = I2C_CLK_SRC_DEFAULT,
    .i2c_port = I2C_NUM_0,
    .sda_io_num = GPIO_NUM_21,
    .scl_io_num = 22,
    .glitch_ignore_cnt = 7,
    .flags.enable_internal_pullup = true,
};

i2c_master_bus_handle_t bus_handle;

i2c_device_config_t dev_cfg = {
    .dev_addr_length = I2C_ADDR_BIT_LEN_7,
    .device_address = 0x76,
    .scl_speed_hz = 100000, /* REVIEW */
};

i2c_master_dev_handle_t dev_handle;

/**
 * @brief peripherals Init
 */
esp_err_t peripheralsInit(void)
{
    esp_err_t esp_err = ESP_FAIL;
    // I2C INITIALIZATION
    ESP_ERROR_CHECK(esp_err = i2c_new_master_bus(&i2c_mst_config, &bus_handle));

    ESP_ERROR_CHECK(esp_err = i2c_master_bus_add_device(bus_handle, &dev_cfg, &dev_handle));

    ESP_ERROR_CHECK(esp_err = configuration_bme280());

    return esp_err;
}

esp_err_t configuration_bme280()
{
    esp_err_t err_ret =  ESP_FAIL;
    /* Interface selection is to be updated as parameter
     * For I2C :  BME280_I2C_INTF
     * For SPI :  BME280_SPI_INTF
     */
    /*! Chip Id */
    dev.chip_id = BME280_CHIP_ID;
    /*! Interface Selection
     * For SPI, intf = BME280_SPI_INTF
     * For I2C, intf = BME280_I2C_INTF
     */
    dev.intf= BME280_I2C_INTF;

    /*!
     * The interface pointer is used to enable the user
     * to link their interface descriptors for reference during the
     * implementation of the read and write interfaces to the
     * hardware.
     */
    dev.intf_ptr = interfacePointerGLobal;

    /*! Variable to store result of read/write function */
    dev.intf_rslt = interfacePointerResult;

    /*! Read function pointer */
    dev.read = i2c_master_bme280_read;

    /*! Write function pointer */
    dev.write = i2c_master_bme280_write;

    /*! Delay function pointer */
    dev.delay_us = BME280_delay_msek;
    /*! Trim data */
    //struct bme280_calib_data calib_data;

    /*ESP_LOGI("I2C","Calib Data T1 [%d]",dev.calib_data.dig_t1);
    ESP_LOGI("I2C","Calib Data T2 [%d]",dev.calib_data.dig_t2);
    ESP_LOGI("I2C","Calib Data T3 [%d]",dev.calib_data.dig_t3);
    */
    err_ret = bme280_init(&dev);

    /*ESP_LOGI("I2C","Calib Data T1 [%d]",dev.calib_data.dig_t1);
    ESP_LOGI("I2C","Calib Data T2 [%d]",dev.calib_data.dig_t2);
    ESP_LOGI("I2C","Calib Data T3 [%d]",dev.calib_data.dig_t3);
    */

    /* Always read the current settings before writing, especially when all the configuration is not modified */
    err_ret = bme280_get_sensor_settings(&settings, &dev);

    /* Configuring the over-sampling rate, filter coefficient and standby time */
    /* Overwrite the desired settings */
    settings.filter = BME280_FILTER_COEFF_2;

    /* Over-sampling rate for humidity, temperature and pressure */
    settings.osr_h = BME280_OVERSAMPLING_1X;
    settings.osr_p = BME280_OVERSAMPLING_1X;
    settings.osr_t = BME280_OVERSAMPLING_1X;

    // /* Setting the standby time */
    settings.standby_time = BME280_STANDBY_TIME_0_5_MS;

    err_ret = bme280_set_sensor_settings(BME280_SEL_ALL_SETTINGS, &settings, &dev);

    // /* Always set the power mode after setting the configuration */
    err_ret = bme280_set_sensor_mode(BME280_POWERMODE_NORMAL, &dev);

    // /* Calculate measurement time in microseconds */
    err_ret = bme280_cal_meas_delay(&period, &settings);

    /*ESP_LOGI("I2C","Calib Data T1 [%d]",dev.calib_data.dig_t1);
    ESP_LOGI("I2C","Calib Data T2 [%d]",dev.calib_data.dig_t2);
    ESP_LOGI("I2C","Calib Data T3 [%d]",dev.calib_data.dig_t3);
    */

   return err_ret;
}


/**
 * @brief test code to write i2c bme280
 *
 * 1. send data
 * ___________________________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | write reg_address + ack | write data_len byte + ack  | stop |
 * --------|---------------------------|-------------------------|----------------------------|------|
 *
 * @param i2c_num I2C port number
 * @param reg_address slave reg address
 * @param data data to send
 * @param data_len data length
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Sending command error, slave doesn't ACK the transfer.
 *     - ESP_ERR_INVALID_STATE I2C driver not installed or not in master mode.
 *     - ESP_ERR_TIMEOUT Operation timeout because the bus is busy.
 */

int8_t i2c_master_bme280_write(uint8_t reg_address, const uint8_t *data, size_t data_len, void *intf_ptr)
{
    esp_err_t err_ret =  ESP_FAIL;
    //ESP_LOGI("I2C", "Write Request");
    uint8_t array[data_len+1];
    array[0] = reg_address;
    for (int i=0;i<data_len+1;i++) {
        array[i+1]=data[i];
    }
    ESP_ERROR_CHECK(err_ret =  i2c_master_transmit(dev_handle,&array , data_len+1, -1));
    //ESP_ERROR_CHECK(ret =  i2c_master_transmit(dev_handle, data, , -1));
    return err_ret;
}

/**
 * @brief test code to read i2c bme280
 *
 * 1. send reg address
 * ______________________________________________________________________
 * | start | slave_addr + wr_bit + ack | write reg_address + ack | stop |
 * --------|---------------------------|-------------------------|------|
 *
 * 2. read data
 * ___________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | read data_len byte + ack(last nack)  | stop |
 * --------|---------------------------|--------------------------------------|------|
 *
 * @param i2c_num I2C port number
 * @param reg_address slave reg address
 * @param data data to read
 * @param data_len data length
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Sending command error, slave doesn't ACK the transfer.
 *     - ESP_ERR_INVALID_STATE I2C driver not installed or not in master mode.
 *     - ESP_ERR_TIMEOUT Operation timeout because the bus is busy.
 */
int8_t i2c_master_bme280_read(uint8_t reg_address, uint8_t *data, size_t data_len, void *intf_ptr)
{
    esp_err_t err_ret =  ESP_FAIL;
    //ESP_LOGI("I2C", "Read Request");
    ESP_ERROR_CHECK(err_ret = i2c_master_transmit_receive(dev_handle,&reg_address,1, data, data_len,-1));
    return err_ret;
}

// /*!
//  *  @brief This internal API is used to get compensated temperature data.
//  */
double * get_tempHumPress()
{
    int8_t rslt = BME280_E_NULL_PTR;
    int8_t idx = 0;
    uint8_t status_reg;
    struct bme280_data comp_data;
    

    while (idx < 1) //SAMPLE_COUNT
    {
        rslt = bme280_get_regs(BME280_REG_STATUS, &status_reg, 1, &dev);
        //bme280_error_codes_print_result("bme280_get_regs", rslt);
        if (status_reg & BME280_STATUS_MEAS_DONE)
        {
            /* Measurement time delay given to read sample */
            //dev.delay_us(period, dev.intf_ptr);
            /* Read compensated data */
            rslt = bme280_get_sensor_data(BME280_PRESS | BME280_TEMP | BME280_HUM, &comp_data, &dev);
            
            idx++;
        }
        idx++;
    }
    double returnData  [3] = {comp_data.temperature,comp_data.humidity,comp_data.pressure/100 };
    double * ret = &returnData;
    return ret;
}