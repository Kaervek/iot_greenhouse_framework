#include <stdio.h>


#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_system.h"
#include "esp_event.h"
//#include "esp_netif.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "organizer.h"
#include "esp_log.h"

#include "peripherals.h"
#include "wirelessCOM.h"

/* LOCAL VARIABLES */
static int EVENT = ESP_INITIALIZED;
static void vTimerCallback(TimerHandle_t pxTimer);
static void readData();
esp_err_t organizerInit(void) {

    esp_err_t err_ret =  ESP_FAIL;
    // 1. TODO First Read from NVM previous stored configuration for WIFI/MQTT

    // 2. TODO If found initialize with those data - IF NOT launch with default BLE loop to configure.

    // 3. WIFI/MQTT Initialization
    err_ret = wirelessCOMInit(/*HERE SHOULD GO INITIALIZATION DATA*/);
    // 4. Peripherals Initialization
    err_ret = peripheralsInit(/*HERE SHOULD GO INITIALIZATION DATA*/);
    // 5. Other Inits
    if(err_ret == ESP_OK) EVENT = ESP_INITIALIZED; //DEVICE INITIALIZED
    // ELSE other events conditions
    return err_ret;

}

void organizerSTM(void) {
    uint32_t timerId = 1;
    TimerHandle_t xTimers;
    esp_err_t err_ret =  ESP_FAIL;
    //while(true){
        switch(EVENT) {
            case ESP_INITIALIZED:
                // Based on the configuration provided starts to create timers
                ESP_LOGI("EO","EOEOEOEOE");
                EVENT = ESP_NORMALOP;
                uint32_t timerId = 1;
                TimerHandle_t xTimers = xTimerCreate("Timer",               // Just a text name, not used by the kernel.
                                                    (pdMS_TO_TICKS(5000)), // The timer period in ticks.
                                                    pdTRUE,                // The timers will auto-reload themselves when they expire.
                                                    (void *)timerId,       // Assign each timer a unique id equal to its array index.
                                                    vTimerCallback         // Each timer calls the same callback when it expires.
                );
                if (xTimers == NULL)
                { // The timer was not created.
                }
                else

                {
                    // Start the timer.  No block time is specified, and even if one was
                    // it would be ignored because the scheduler has not yet been
                    // started.
                    if (xTimerStart(xTimers, 0) != pdPASS)
                    {
                        // The timer could not be set into the Active state.
                    }
                }
                break;
            case ESP_NORMALOP:
                ESP_LOGI("EO","AAAAA");
                
                
                break;
            case ESP_OTA:
                break;
            case ESP_DEINIT:
                break;
            default:
                err_ret = ESP_OK;
                
                
                break;
        //}

    }

    /*if (xTimers == NULL)
    { // The timer was not created.
    }
    else

    {
        // Start the timer.  No block time is specified, and even if one was
        // it would be ignored because the scheduler has not yet been
        // started.
        if (xTimerStart(xTimers, 0) != pdPASS)
        {
            // The timer could not be set into the Active state.
        }
    }*/

    vTaskDelete(NULL);
    
}


static void vTimerCallback(TimerHandle_t pxTimer)
{    
    ESP_LOGI("Timer5s", "Task Timer 5s");
    xTaskCreate(readData, "readData", 2024, NULL, 1, NULL);
    
}

void readData()  {
    //double * value  = get_tempHumPress();
    
    //ESP_LOGI("ORGANIZER","%f    %f   %f",value[0],value[1],value[2]);
    publishTopics(get_tempHumPress(),3);
    vTaskDelete(NULL);

}