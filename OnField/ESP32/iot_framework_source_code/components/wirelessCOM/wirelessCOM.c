// C program for implementation of ftoa() 
#include <math.h> 
#include <stdio.h>
#include "wirelessCOM.h"
#include "string.h"
/* MQTT AND WIFI CONNECTION */
#include "protocol_examples_common.h"
#include "mqtt_client.h"
#include "esp_wifi.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"


/* GLOBAL VARIABLES */
static esp_mqtt_client_handle_t clientGlobal;
const esp_mqtt_client_config_t mqtt_cfg = {
        .broker.address.uri = CONFIG_BROKER_URI,
    };

struct topic {
    char name[50];//"esp8266/bme280/temperature",
    int time;
};

struct topicsToPublish {
    struct topic listOfTopics[3];
};

struct MQTTConfig {
    struct topicsToPublish ToPublish;
    //struct topicsToSubscribe ToSubscribe;
};
static struct topic TOPICSP[3];
/* LOCAL FUNCTIONS DECLARATION */
static esp_err_t mqtt_app_start(void);
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
static void log_error_if_nonzero(const char *message, int error_code);
static void ftoa(float n, char* res, int afterpoint);
static int intToStr(int x, char str[], int d);
static void reverse(char* str, int len);

/* PUBLIC FUNCTIONS */
/// @brief 
/// @param  
/// @return 
esp_err_t wirelessCOMInit(void){
    esp_err_t err_ret =  ESP_FAIL;
    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(err_ret = example_connect());
    
    ESP_ERROR_CHECK(err_ret = mqtt_app_start());
    // TODO BASED ON CONFIGURATION PASSED FOR INITIALIZATION 
    // ARRAY FOR SUBSCRIPTION
    // ARRAY FOR TOPICS - TIMMINGS
    strcpy(TOPICSP[0].name,"esp8266/bme280/temperature");
    TOPICSP[0].time=5000;
    strcpy(TOPICSP[1].name,"esp8266/bme280/humidity");
    TOPICSP[1].time=5000;
    strcpy(TOPICSP[2].name,"esp8266/bme280/pressure");
    TOPICSP[2].time=5000;


    return err_ret;
}

esp_err_t wirelessCOMDeInit(){
    //TODO- we can stablish a partial deinitialization
    ESP_LOGI("A","AA");
    return ESP_FAIL;
}

/* LOCAL FUNCTIONS */
/// @brief  mqtt_app_start
/// @param  none
/// @return ESP_ERROR_STATUS 
static esp_err_t mqtt_app_start(void)
{
    esp_err_t err_ret =  ESP_FAIL;
    
    clientGlobal = esp_mqtt_client_init(&mqtt_cfg);
    /* The last argument may be used to pass data to the event handler, in this example mqtt_event_handler */
    ESP_ERROR_CHECK(err_ret = esp_mqtt_client_register_event(clientGlobal, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL));
    ESP_ERROR_CHECK(err_ret = esp_mqtt_client_start(clientGlobal));

    return err_ret;
}

/*
 * @brief Event handler registered to receive MQTT events
 *
 *  This function is called by the MQTT client event loop.
 *
 * @param handler_args user data registered to the event.
 * @param base Event base for the handler(always MQTT Base in this example).
 * @param event_id The id for the received event.
 * @param event_data The data for the event, esp_mqtt_event_handle_t.
 */
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    // TAG "IOT GREENHOUSE ESP32"
    ESP_LOGD("IOT GREENHOUSE ESP32", "Event dispatched from event loop base=%s, event_id=%" PRIi32, base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id) {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_CONNECTED");
        /* TODO HERE FIRSTLY SHALL READ THE NVM FOR CONFIGURED*/
        /* WE HAVE TO SUBSCRIBE TO A SERIES OF ACTUATORS OR CHANGE OF FUNCTIONALITY */
        /*msg_id = esp_mqtt_client_publish(client, "/topic/qos1", "data_3", 0, 1, 0);

        ESP_LOGI("IOT GREENHOUSE ESP32", "sent publish successful, msg_id=%d", msg_id);
        msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
        ESP_LOGI("IOT GREENHOUSE ESP32", "sent subscribe successful, msg_id=%d", msg_id);

        msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", 1);
        ESP_LOGI("IOT GREENHOUSE ESP32", "sent subscribe successful, msg_id=%d", msg_id);

        msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
        ESP_LOGI("IOT GREENHOUSE ESP32", "sent unsubscribe successful, msg_id=%d", msg_id);*/
        break;
    case MQTT_EVENT_DISCONNECTED:
        // TAG "IOT GREENHOUSE ESP32"
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
        ESP_LOGI("IOT GREENHOUSE ESP32", "sent publish successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_DATA");
        printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
        printf("DATA=%.*s\r\n", event->data_len, event->data);
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI("IOT GREENHOUSE ESP32", "MQTT_EVENT_ERROR");
        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
            log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
            log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
            log_error_if_nonzero("captured as transport's socket errno",  event->error_handle->esp_transport_sock_errno);
            ESP_LOGI("IOT GREENHOUSE ESP32", "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));

        }
        break;
    default:
        ESP_LOGI("IOT GREENHOUSE ESP32", "Other event id:%d", event->event_id);
        break;
    }
}



static void log_error_if_nonzero(const char *message, int error_code)
{
    if (error_code != 0) {
        ESP_LOGE("IOT GREENHOUSE ESP32", "Last error %s: 0x%x", message, error_code);
    }
}


// Reverses a string 'str' of length 'len' 
static void reverse(char* str, int len) 
{ 
    int i = 0, j = len - 1, temp; 
    while (i < j) { 
        temp = str[i]; 
        str[i] = str[j]; 
        str[j] = temp; 
        i++; 
        j--; 
    } 
} 
 
// Converts a given integer x to string str[]. 
// d is the number of digits required in the output. 
// If d is more than the number of digits in x, 
// then 0s are added at the beginning. 
static int intToStr(int x, char str[], int d) 
{ 
    int i = 0; 
    while (x) { 
        str[i++] = (x % 10) + '0'; 
        x = x / 10; 
    } 
 
    // If number of digits required is more, then 
    // add 0s at the beginning 
    while (i < d) 
        str[i++] = '0'; 
 
    reverse(str, i); 
    str[i] = '\0'; 
    return i; 
} 
 
// Converts a floating-point/double number to a string. 
static void ftoa(float n, char* res, int afterpoint) 
{ 
    // Extract integer part 
    int ipart = (int)n; 
 
    // Extract floating part 
    float fpart = n - (float)ipart; 
 
    // convert integer part to string 
    int i = intToStr(ipart, res, 0); 
 
    // check for display option after point 
    if (afterpoint != 0) { 
        res[i] = '.'; // add dot 
 
        // Get the value of fraction part upto given no. 
        // of points after dot. The third parameter 
        // is needed to handle cases like 233.007 
        fpart = fpart * pow(10, afterpoint); 
 
        intToStr((int)fpart, res + i + 1, afterpoint); 
    } 
} 

// REQUIRE API THAT DEPLOYS PUBLISH - SUBSCRIBES OR HOW TO USE THE ACTUAL ONES
esp_err_t publishTopics(double * value,int len) {
    //esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/temperature", "25", 0, 0, 0);
    //esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/temperature", "20", 0, 0, 0);
    ESP_LOGI("PUBLISH","%f",value[0]);
    ESP_LOGI("PUBLISH","%f",value[1]);
    ESP_LOGI("PUBLISH","%f",value[2]);
    for(int i=0;i<3;i++){
        
        char res[20];
        ftoa(value[i], res, 4);
        ESP_LOGI("PUBLISH","%s",TOPICSP[i].name);
        esp_mqtt_client_publish(clientGlobal, TOPICSP[i].name, res, 0, 0, 0);
        vTaskDelay(100);

    }
    return ESP_OK;
}
/*
static void i2c_task_example (void *arg) {
    ESP_LOGI(TAG, "Task i2c_task_example reached");
    value = get_tempHumPress(); // Call to component Peripherals
    char res[20]; 
    ESP_LOGI("I2C","-----------------------");
    ESP_LOGI("I2C","Temperature");
    ftoa(value.temperature, res, 4); 
    ESP_LOGI("I2C","A %s deg C\n", res);
    esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/temperature", res, 0, 0, 0);
    ESP_LOGI("I2C","-----------------------");
    ESP_LOGI("I2C","Humidity");
    ftoa(value.humidity, res, 4);
    ESP_LOGI("I2C","A %s porciento \n", res);
    esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/humidity",res, 0, 0, 0);
    ESP_LOGI("I2C","-----------------------");
    ESP_LOGI("I2C","Pressure");
    ftoa(value.pressure/100, res, 4);
    ESP_LOGI("I2C","A %s Pa\n", res);
    esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/pressure", res, 0, 0, 0);
    vTaskDelete(NULL);
}*/

/*static void vTimerCallback(TimerHandle_t pxTimer)
{    
    ESP_LOGI(TAG, "Timer callback reached");
    //xTaskCreate(i2c_task_example, "i2c_task_example", 4048, NULL, 1, NULL);
    xTaskCreate(organizerInit, "organizerInit", 2024, NULL, 1, NULL);
    
}*/