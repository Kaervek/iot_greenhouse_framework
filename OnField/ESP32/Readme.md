<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]
-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!--<a href="https://github.com/othneildrew/Best-README-Template">-->
    <img src="images/esp32_logo.png" alt="Logo" width="100" height="100">
  </a>

  <h3 align="center">ESP32 On Field</h3>

  <p align="center">
    Readme for development with ESP32 board and usage of docker images to build and  flash
    <br />
    <!--
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues/new?labels=bug&template=bug-report---.md">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues/new?labels=enhancement&template=feature-request---.md">Request Feature</a>
    -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#documentation">Documentation</a></li>
    <!--
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
    -->
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
    <img src="images/esp32_board.png" alt="Logo" width="250" height="200">
</div>
<p>
This sub-project goal is to develop the  API to integrate the ESP32 into the IOT Greenhouse application.

Main Features:
* Read from a set of sensors via SPI/I2C/BLE/RF/...
* Send information read from sensors to server via MQTT messages
* Perform operations requested by server via MQTT into actuators
* OTA Flash and configurable

</p>

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

This section list any major frameworks/libraries used to bootstrap this project.

[Espressif IDF Framework](https://idf.espressif.com/)
<!--Leave any add-ons/plugins for the acknowledgements section.-->
<!--
* [![Next][Next.js]][Next-url]
* [![React][React.js]][React-url]
* [![Vue][Vue.js]][Vue-url]
* [![Angular][Angular.io]][Angular-url]
* [![Svelte][Svelte.dev]][Svelte-url]
* [![Laravel][Laravel.com]][Laravel-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![JQuery][JQuery.com]][JQuery-url]
-->
<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This project is based on [ESP32 IDF documentation](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/index.html).
 
In order to compile and flash your board we will use a docker image, espressif alredy provides their [own official image](https://hub.docker.com/r/espressif/idf). Find specific documentation and usage [here](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/tools/idf-docker-image.html).

To get a local copy up and running follow these simple example steps.

### Prerequisites

List of things you need to use the software and how to install them.

* [docker](https://docs.docker.com/engine/install/)
  <!--```sh
  npm install npm@latest -g
  ```-->
* 5 GB for docker image
* [ESP32-DevKitC V4](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/hw-reference/esp32/get-started-devkitc.html#get-started-esp32-devkitc-board-front) or equivalent

### Installation

_Below is an example of how you can install and set up the development environment._

1. Get stable image [espressif/idf](https://hub.docker.com/r/espressif/idf)

   ```
   $ sudo docker pull espressif/idf
    [sudo] password for <user>: 
    Using default tag: latest
    latest: Pulling from espressif/idf
    49b384cc7b4a: Pull complete 
    f06a9c6dea87: Pull complete 
    3248dfbeb8a9: Pull complete 
    7bb60fb3c508: Pull complete 
    eb4c6a2a5996: Pull complete 
    79396d0c2f1d: Pull complete 
    f15d38b02862: Pull complete 
    4f4fb700ef54: Pull complete 
    Digest: sha256:128f01e878e93f56e33f9161c27994de984744ddb784a275be93165351798233
    Status: Downloaded newer image for espressif/idf:latest
    docker.io/espressif/idf:latest
   $ sudo docker images | grep espressif
    espressif/idf               latest    548f67c360eb   26 hours ago    4.83GB
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

_The main usage of the previous downloaded docker image is to build the source code and flash it._

Further documentation how to use espressif docker image can be found [here](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/tools/idf-docker-image.html)

The makefile on root folder from ESP32 does the job for us:


1. Set environment variables under env.json file.
   ```
   {
    "wifi_ssid":"<Insert your Wifi SSID here>",
    "wifi_password":"<Insert your Wifi Passowrd here>",
    "mqtt_server":"192.168.XX.XXX"
    }
   ```

### CI - Background  Mode

2. Build image from source project folder, run command under ESP32 directory.
   ```
   make build_linux
   ```

It will generate a build folder with all the object files under  iot_fraemwork_source_code directory.

3. Plug Device via USB and ensure is recognized _(only for Linux OS or  MAC OS - adapt of makefile command can be required as now it is pointing to /device/ttyUSB0)_
4. Flash image from source project folder, run command under ESP32 directory. _(only for Linux OS or  MAC OS)_
   ```
   make flash_linux
   ```

It will start flashing the device and reseting it. No button press or other action required.

### Interactive Mode

2. Run container and attach to it.
   ```
   make run_espimage_linux_it
   ```
This will provide access to the container image and build/flash/monitor commands from idf tool can be  use as usual, please refer to more info for this commands [here](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/tools/idf-py.html)

Basic usage : Under /project  folder

Start the Graphical Configuration Tool
   ```
   idf.py menuconfig
   ```
Build
   ```
   idf.py build
   ```
Clean
   ```
   idf.py clean
   ```
Flash
   ```
   idf.py flash
   ```
Monitor
   ```
   idf.py monitor
   ```
   _Note to exit the monitor use Ctrl+X+T or terminate the terminal_

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### GIT Management for sdkconfig/env.json or other files private files

As sdkconfig is a file that is usually changed for IPs or data that is private as passwords or WIFI SSIDs, that data does not want to be included. 

For this those are excluded under the git repo to avoid mistakes.

```
  git update-index --no-assume-unchanged pathTo/fileToExclude # include again to check changes
  git update-index --assume-unchanged pathTo/fileToExclude  # ignore further changes to a file
```

<!-- ROADMAP -->
## Roadmap

- [x] Add Readme
- [x] Include Development Docker Image setup
- [x] Add Base Example of MQTT
- [ ] Sensor 
    - [ ] BME280 Integration via I2C
    - [ ] BME680 Integration via I2C
    - [ ] Generic I2C Device controller
- [ ] Actuators
    - [ ] Relays controller
    - [ ] Motor Board controller IBT_2
    - [ ] Generic PWM controller
- [ ] BLE
    - [ ] BLE example setup - initial wifi configuration
    - [ ] BLE mesh and sensors data receiver


See the [open issues](https://gitlab.com/Kaervek/iot_greenhouse_framework/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- Documentation -->
## Documentation

[Architecture design](./docs/architecture.md)

<p align="right">(<a href="#readme-top">back to top</a>)</p>
<!-- CONTRIBUTING 
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>
-->


<!-- LICENSE -->
## License

As an open source project it  applies the GNU General Public License (GPL)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

[Kaervek](https://www.reddit.com/user/NoSea1819/) - Reddit user - chat here or via email hugomallo@hotmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS 
## Acknowledgments

Use this space to list resources you find helpful and would like to give credit to. I've included a few of my favorites to kick things off!

* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Malven's Flexbox Cheatsheet](https://flexbox.malven.co/)
* [Malven's Grid Cheatsheet](https://grid.malven.co/)
* [Img Shields](https://shields.io)
* [GitHub Pages](https://pages.github.com)
* [Font Awesome](https://fontawesome.com)
* [React Icons](https://react-icons.github.io/react-icons/search)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

-->

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links 
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
[Angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[Angular-url]: https://angular.io/
[Svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[Svelte-url]: https://svelte.dev/
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 
-->