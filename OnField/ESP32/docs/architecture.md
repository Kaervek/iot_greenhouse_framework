<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!-- PROJECT SHIELDS -->
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!--<a href="https://github.com/othneildrew/Best-README-Template">-->
    <img src="../images/esp32_logo.png" alt="Logo" width="100" height="100">
  </a>

  <h3 align="center">ESP32 Architecture</h3>

  <p align="center">
    Architecture design for HW/SW development with ESP32 board
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#design-principles">Design principles</a>
      <!--ul>
        <li><a href="#built-with">Built With</a></li>
      </ul-->
    </li>
    <li><a href="#sw-architecture">SW Architecture</a></li>
    <li><a href="#hw-architecture">HW Architecture</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## Design principles

<p>
This project follows the following principles/patterns.

Main Principles:
* a
* b
* c
* d

</p>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<div>
    <img src= "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAeEAAACPCAYAAADNw2OQAAAAAXNSR0IArs4c6QAACIJ0RVh0bXhmaWxlACUzQ214ZmlsZSUzRSUzQ2RpYWdyYW0lMjBpZCUzRCUyMkJZRlBQWnAtWWxsdnBNZks3UFE2JTIyJTIwbmFtZSUzRCUyMlBhZ2UtMSUyMiUzRSUzQ214R3JhcGhNb2RlbCUyMGR4JTNEJTIyMTA2MCUyMiUyMGR5JTNEJTIyNTkzJTIyJTIwZ3JpZCUzRCUyMjElMjIlMjBncmlkU2l6ZSUzRCUyMjEwJTIyJTIwZ3VpZGVzJTNEJTIyMSUyMiUyMHRvb2x0aXBzJTNEJTIyMSUyMiUyMGNvbm5lY3QlM0QlMjIxJTIyJTIwYXJyb3dzJTNEJTIyMSUyMiUyMGZvbGQlM0QlMjIxJTIyJTIwcGFnZSUzRCUyMjElMjIlMjBwYWdlU2NhbGUlM0QlMjIxJTIyJTIwcGFnZVdpZHRoJTNEJTIyODUwJTIyJTIwcGFnZUhlaWdodCUzRCUyMjExMDAlMjIlMjBtYXRoJTNEJTIyMCUyMiUyMHNoYWRvdyUzRCUyMjAlMjIlM0UlM0Nyb290JTNFJTNDbXhDZWxsJTIwaWQlM0QlMjIwJTIyJTJGJTNFJTNDbXhDZWxsJTIwaWQlM0QlMjIxJTIyJTIwcGFyZW50JTNEJTIyMCUyMiUyRiUzRSUzQ214Q2VsbCUyMGlkJTNEJTIyQW9oSjd0bzdMand6TllabVllYU0tMSUyMiUyMHZhbHVlJTNEJTIyJTIyJTIwc3R5bGUlM0QlMjJyb3VuZGVkJTNEMCUzQndoaXRlU3BhY2UlM0R3cmFwJTNCaHRtbCUzRDElM0IlMjIlMjB2ZXJ0ZXglM0QlMjIxJTIyJTIwcGFyZW50JTNEJTIyMSUyMiUzRSUzQ214R2VvbWV0cnklMjB4JTNEJTIyMzMwJTIyJTIweSUzRCUyMjMyMCUyMiUyMHdpZHRoJTNEJTIyMTQwJTIyJTIwaGVpZ2h0JTNEJTIyNzAlMjIlMjBhcyUzRCUyMmdlb21ldHJ5JTIyJTJGJTNFJTNDJTJGbXhDZWxsJTNFJTNDbXhDZWxsJTIwaWQlM0QlMjJBb2hKN3RvN0xqd3pOWVptWWVhTS0yJTIyJTIwdmFsdWUlM0QlMjIlMjZsdCUzQmgxJTI2Z3QlM0JFU1AzMiUyNmx0JTNCJTJGaDElMjZndCUzQiUyNmx0JTNCcCUyNmd0JTNCRVNQMzItRGV2S2l0QyUyNmFtcCUzQm5ic3AlM0IlMjBWNCUyNmFtcCUzQm5ic3AlM0IlMjZsdCUzQiUyRnAlMjZndCUzQiUyMiUyMHN0eWxlJTNEJTIydGV4dCUzQmh0bWwlM0QxJTNCc3Ryb2tlQ29sb3IlM0Rub25lJTNCZmlsbENvbG9yJTNEbm9uZSUzQnNwYWNpbmclM0Q1JTNCc3BhY2luZ1RvcCUzRC0yMCUzQndoaXRlU3BhY2UlM0R3cmFwJTNCb3ZlcmZsb3clM0RoaWRkZW4lM0Jyb3VuZGVkJTNEMCUzQiUyMiUyMHZlcnRleCUzRCUyMjElMjIlMjBwYXJlbnQlM0QlMjIxJTIyJTNFJTNDbXhHZW9tZXRyeSUyMHglM0QlMjIzMzUlMjIlMjB5JTNEJTIyMzIwJTIyJTIwd2lkdGglM0QlMjIxMjUlMjIlMjBoZWlnaHQlM0QlMjI3MCUyMiUyMGFzJTNEJTIyZ2VvbWV0cnklMjIlMkYlM0UlM0MlMkZteENlbGwlM0UlM0NteENlbGwlMjBpZCUzRCUyMkFvaEo3dG83TGp3ek5ZWm1ZZWFNLTMlMjIlMjB2YWx1ZSUzRCUyMiUyMiUyMHN0eWxlJTNEJTIyc2hhcGUlM0RsaW5rJTNCaHRtbCUzRDElM0IlMjIlMjBlZGdlJTNEJTIyMSUyMiUyMHBhcmVudCUzRCUyMjElMjIlM0UlM0NteEdlb21ldHJ5JTIwd2lkdGglM0QlMjIxMDAlMjIlMjByZWxhdGl2ZSUzRCUyMjElMjIlMjBhcyUzRCUyMmdlb21ldHJ5JTIyJTNFJTNDbXhQb2ludCUyMHglM0QlMjI0MjAlMjIlMjB5JTNEJTIyMjYwJTIyJTIwYXMlM0QlMjJzb3VyY2VQb2ludCUyMiUyRiUzRSUzQ214UG9pbnQlMjB4JTNEJTIyNDIwJTIyJTIweSUzRCUyMjMyMCUyMiUyMGFzJTNEJTIydGFyZ2V0UG9pbnQlMjIlMkYlM0UlM0MlMkZteEdlb21ldHJ5JTNFJTNDJTJGbXhDZWxsJTNFJTNDbXhDZWxsJTIwaWQlM0QlMjJBb2hKN3RvN0xqd3pOWVptWWVhTS01JTIyJTIwdmFsdWUlM0QlMjIlMjIlMjBzdHlsZSUzRCUyMnNoYXBlJTNEbGluayUzQmh0bWwlM0QxJTNCJTIyJTIwZWRnZSUzRCUyMjElMjIlMjBwYXJlbnQlM0QlMjIxJTIyJTNFJTNDbXhHZW9tZXRyeSUyMHdpZHRoJTNEJTIyMTAwJTIyJTIwcmVsYXRpdmUlM0QlMjIxJTIyJTIwYXMlM0QlMjJnZW9tZXRyeSUyMiUzRSUzQ214UG9pbnQlMjB4JTNEJTIyMTkwJTIyJTIweSUzRCUyMjI2MCUyMiUyMGFzJTNEJTIyc291cmNlUG9pbnQlMjIlMkYlM0UlM0NteFBvaW50JTIweCUzRCUyMjY2MCUyMiUyMHklM0QlMjIyNjAlMjIlMjBhcyUzRCUyMnRhcmdldFBvaW50JTIyJTJGJTNFJTNDJTJGbXhHZW9tZXRyeSUzRSUzQyUyRm14Q2VsbCUzRSUzQyUyRnJvb3QlM0UlM0MlMkZteEdyYXBoTW9kZWwlM0UlM0MlMkZkaWFncmFtJTNFJTNDJTJGbXhmaWxlJTNFkxeHKAAAGBpJREFUeF7tnXm0TeX/xz+Z5yhDpjLPRVlKSjJkiFaJskRJ5qwQiwxlKsMyVJQpQ2RopRKhUhnKsFaSkMyhzAmhzMP3+35+v32+5xznuuc8d9/H3s9+P/9w73nG12ff+9rPsPe9SZhIgARIgARIgARuCIGbbkirbJQESIAESIAESEAoYV4EJEACJEACJHCDCFDCNwg8myUBEiABEiABSpjXAAmQAAmQAAncIAJJSXjAf/sz8Ab1ic2SAAmQAAmQgG0E4NRB0YPiTNi2MHM8JEACJEACviFACfsmVOxogAlgZSr8Djr66wCj4dBJwN8EKGF/x4+9DwaBq/+VcPjPavTXwaDAUZKAhQS4J2xhUDkkEiABEiABzxHgnrDnQsIOkUB8BDgTjo8Tc5GA7whwOdp3IWOHA0iAEg5g0DnkYBCghIMRZ47S3wQoYX/Hj70ngSQJUMK8OEjA+wQoYe/HiD0kAS0ClLAWNhYiAaMEKGGjuNkYCZgjQAmbY82WSECXACWsS47lSMDjBChhjweI3SMBEaGEeRmQgKUEKGFLA8thWUWAErYqnBwMCfyPACXMq4EEvE+AEvZ+jNhDEtAiQAlrYWMhEjBKgBI2ipuNkYA5ApSwOdZsiQR0CVDCuuRYjgQ8ToAS9niA2D0S4MEsXgMkYC8BStje2HJk9hDgTNieWHIkJBBBgBLmBUEC3idACXs/RuwhCWgRoIS1sLEQCRglQAkbxc3GSMAcAUrYHGu2RAK6BChhXXIsRwIeJ0AJezxA7B4J8GAWrwESsJcAJWxvbDkyewhwJmxPLDkSEoggQAnzgiAB7xOghL0fI/aQBLQIUMJa2FiIBIwSoISN4mZjJGCOACVsjjVbIgFdApSwLjmWIwGPE6CEPR4gdo8EeDCL1wAJ2EuAErY3thyZPQQ4E7YnlhwJCUQQoIR5QZCA9wlQwt6PEXtIAloEKGEtbCxEAkYJUMJGcbMxEjBHgBI2x5otkYAuAUpYlxzLkYDHCVDCHg8Qu0cCPJjFa4AE7CVACdsbW47MHgKcCdsTS46EBCIIUMK8IEjA+wQoYe/HiD0kAS0ClLAWNhYiAaMEKGGjuNkYCZgjQAmbY82WSECXACWsS47lSMDjBChhjweI3SMBHsziNUAC9hKghO2NLUdmDwHOhO2JJUdCAhEEKGFeECTgfQKUsPdjxB6SgBYBSlgLGwuRgFEClLBR3GyMBMwRoITNsWZLJKBLgBLWJcdyJOBxApSwxwPE7pEAD2bxGiABewlQwvbGliOzhwBnwvbEkiMhgQgClDAvCBLwPgFK2PsxYg9JQIsAJayFjYVIwCgBStgobjZGAuYIUMLmWLMlEtAlQAnrkmM5EvA4AUrY4wFi90iAB7N4DZCAvQQoYXtjy5HZQ4AzYXtiyZGQQAQBSpgXBAl4nwAl7P0YsYckoEWAEtbCxkIkYJQAJWwUNxsjAXMEKGFzrNkSCegSoIR1ybEcCXicACXs8QCxeyTAg1m8BkjAXgKUsL2x5cjsIcCZsD2x5EhIIIIAJcwLggS8T4AS9n6M2EMS0CJACWthYyESMEqAEjaKm42RgDkClLA51myJBHQJUMK65FiOBDxOgBL2eIDYPRLgwSxeAyRgLwFK2N7YcmT2EOBM2J5YciQkEEGAEuYFQQLeJ0AJez9G7CEJaBGghLWwsRAJGCVACRvFzcZIwBwBStgca7ZEAroEKGFdcixHAh4nQAl7PEDsHgnwYBavARKwlwAlbG9sTY4MMzUmEkiEAH/3JEKLea0lwB8Ea0NrdGBXr16lh40S93FjN92kfu3wd4+PY8iuu0eAPwjusQxyTZRwkKOf4Ngp4QSBMbvVBChhq8NrbHCUsDHU/m+IEvZ/DDkC9whQwu6xDHJNlHCQo5/g2CnhBIExu9UEKGGrw2tscJSwMdT+b4gS9n8MOQL3CFDC7rEMck2UcJCjn+DYKeEEgTG71QQoYavDa2xwlLAx1P5viBL2fww5AvcIUMLusQxyTZRwkKOf4Ngp4QSBMbvVBChhq8NrbHCUsDHU/m+IEvZ/DDkC9whQwu6xDHJNlHCQo5/g2CnhBIExu9UEKGGrw2tscHFLeMqUKdKuXbuEOtahQweZOHFiRJljx47J5MmTZcmSJbJ161b5+++/5cqVK5IjRw4pUaKE1KxZUzp16iS33357RLnk2s+UKZPkzZtX7rvvPmnbtq3UrVs3Zl/nzZsnqGvdunVy4sQJyZYtm5QtW1aaNm0qL774oqCe6IR+jho1SpYtWyYHDx6UdOnSyR133CH16tWTHj16SKFChRLi4tfMlLBfI8d+pwYBSjg1qAavTqMSXr58uTRp0kTJ73opS5YsMnfuXGnYsGEoW3ISjq6vTZs2Svb/Lw65fPmytGrVSmbPnp1k0xUqVFCizZMnTyjPggULpFmzZnL+/PmY5XLmzKluKO69917rrx5K2PoQc4AJEKCEE4DFrEkS0Jbwo48+KunTp78u2vr160vHjh1VnuPHj0vJkiXVv0hZs2aVxx57TM1+IcgNGzYomWFWjJQ9e3bZvXu35M6dW30dLWHMQjEjRUIZzFA3b96s6nLS6NGjpXv37upLzGR79uwZ+qxMmTJSrlw5+eWXX2Tnzp2h7z/33HMyY8YM9fXhw4elVKlScvr06VCfatSoIWfOnJEVK1aE+lq8eHHZvn27pE2b1upLjRK2OrwcXIIEKOEEgTF7TALaEsZsFrPAeNO0adMEs1MkyGrjxo1Svnz5iOLz58+Xxo0bh74HcWK5N5aEY7W/ZcsWqVWrlhw5ckSVKViwoOzfv1/wRyqwZAxRI2G5+r333lOzZEj7ySeflM8//1x9hpuDf/75R/1/2LBh0rdv35CA0eeiRYuqrzFTxwzZSWvWrJH7778/Xhy+zEcJ+zJs7HQqEaCEUwlswKo1JuGBAwfKoEGDFF6IDLPcWGnIkCGSOXNmKVasmFSqVEmKFCkSt4SRceTIkdKrV69Q1fv27RMsb2O/10loA7NXJ4ULFaI5e/asZMyYUdq3b6+Wp5EaNWokb7/9dqjMpUuX1P6xM/OeM2eONG/e3OrLhxK2OrwcXIIEKOEEgTF7TALGJDxhwoSQCNOkSaPkhqXdeFP0cnRSM/HoGSoOVWHp+XppzJgx0q1bN5UF0t+zZ0+y3cJSdf78+UP5Fi9eLFiitzlRwjZHl2NLlAAlnCgx5o9FwJiEse+KPVjMIJEgYswucfiqevXqSpTOIapYHY1XwoMHD5YBAwaEqjh16pTaX46VIHIcFsOpb2evGqe5cao7udS5c2cZP368ypYhQwa1BJ7I8nxy9Xvxc0rYi1Fhn24UAUr4RpG3q11tCSd3MAuPC2HfNTwNHz5c+vTpE5MgBAYZo17stebKlSsiXzwSxuEuPOKEx56QqlWrJqtXr76mPTyeVKVKlYjv58uXT7Bk7hwku16YIV9I2EnYt8b+te2JErY9whxfIgQo4URoMW9SBLQlnBxSPEe7d+/ea7LhBPTQoUNl5cqV6sBUrIRnhpEnXHTREg6/CUA9Bw4cUCesnT1aCANLxA0aNEhWwngkqWvXrmpPF3vR10voV79+/UJZqlatqmbTsZ4vTo6R3z6nhP0WMfY3NQlQwqlJNzh1G5ewgxZ7qkuXLpVVq1YJThbjUaFoKc+cOVNatmypiiTynDAeXRoxYoS8/PLLMSP522+/qVPXJ0+eVC8McU5T4wAX9q7xmFJ0gtwxS0Y/nIQ97YULFya53G3bZUQJ2xZRjiclBCjhlNBjWYeAtoQTfUQpOeR//vmnerkG9nQvXLigsuNNVnjsKB4JYyZaoEABefjhh9UM+p577kmuSfX5xYsX1TI0ZrhIEPiOHTtCjyLhe+gPXjKyaNGiUJ0tWrRQQg7CDNgZNCUc1yXFTAEhQAkHJNCpPEzjEob0ILqkDmFFL/ficSGILp494XhYYUYb/VINHBbD4a1z586pKrDni1dnIuGzp556SvAMMxL6jT727t07nuasykMJWxVODiaFBCjhFAJkcUXAiITxTDDeVoVZ7a5du5TQwl9JGR6L6dOnS+vWrUPfckPC2BvG8vPRo0cFp6Ux6w4/+IU3buElHY6Ew18Sgn47h65w84CXjjz77LOBvHwo4UCGnYNOggAlzEvDDQJGJAy54e1VzmNAhQsXVnupFStWjBgD5IhXXf7888/q+6VLl5Zt27ap/6dkJow6sLTtJDyCNG7cuNCMeOzYsepglpPw9iy8UhOvpsQbuJy96vDXYLoB3291UMJ+ixj7m5oEKOHUpBucurUlnNwjSg5CHK7CUu+kSZOuefyncuXK6jQy3kF96NAh9TiRsx/siNd51WVKJIy6INXwPV3cCOC5ZbzW8tdffw1FHK+3xJ4w3tqFv8i0du3a0Gd16tRJ8v3QtWvXjng3tY2XECVsY1Q5Jl0ClLAuOZYLJ6At4XgxYgnY+SMM2Evt379/xB9ZiFUP9mxfe+21iJdupFTC6Adm2evXr0+y61iixtK18w5o9Bt/ejGehINas2bNiierb/NQwr4NHTueCgQo4VSAGsAqjUoYfPF4EPZVsdSLGSceE0K6+eab1V9Zeuihh9SeMJaiw1NKJYy6cChs6tSp6o8vbNq0Sb3UAzNe/CUnCLpLly4Rr6KkhCN/IijhAP6G4JCTJEAJ8+Jwg0DcEnajMdbhbwKUsL/jx967S4ASdpdnUGujhIMaeY1xU8Ia0FjEWgKUsLWhNTowStgobn83Rgn7O37svbsEKGF3eQa1Nko4qJHXGDclrAGNRawlQAlbG1qjA6OEjeL2d2OUsL/jx967S4ASdpdnUGujhIMaeY1xU8Ia0FjEWgKUsLWhNTowStgobn83Rgn7O37svbsEKGF3eQa1Nko4qJHXGDclrAGNRawlQAlbG1qjA6OEjeL2d2OUsL/jx967S4ASdpdnUGujhIMaeY1xU8Ia0FjEWgKUsLWhNTowStgobn83Rgn7O37svbsEKGF3eQa1Nko4qJHXGDclrAGNRawlQAlbG1qjA6OEjeL2d2OUsL/jx967S4ASdpdnUGujhIMaeY1xU8Ia0FjEWgKUsLWhNTowStgobn83Rgn7O37svbsEKGF3eQa1Nko4qJHXGDclrAGNRawlQAlbG1qjA6OEjeL2d2OUsL/jx967S4ASdpdnUGujhIMaeY1xU8Ia0FjEWgKUsLWhNTowStgobn83Rgn7O37svbsEKGF3eQa1Nko4qJHXGDclrAGNRawlQAlbG1qjA6OEjeL2d2OUsL/jx967S4ASdpdnUGujhIMaeY1xU8Ia0FjEWgKUsLWhNTowStgobn83Rgn7O37svbsEKGF3eQa1Nko4qJHXGDclrAGNRawlQAlbG1qjA6OEjeL2d2OUsL/jx967S4ASdpdnUGuLS8KXLl2S9OnTS9q0aa/htHTpUqlRo4Zs3rxZevToIRs3bpTLly9LkSJFZOjQofLII4+IUz5jxoyqfJo0aaRUqVIyYsQIqVu3rvrezJkzZeDAgfLXX39JpUqVZPLkySoP0uuvvy4TJkyQ8+fPq/z4LFu2bNf05cEHH5S1a9eq+tHXChUqSNeuXeWZZ57Rju+3334rHTt2lF27doXqaNu2rRw9elTmzZsnb731lmzbtk2mTJkiyFumTBkpVKiQynvy5Enp27evfPbZZ3L8+HEpWrSodOrUSbp06XLd/jzwwAPSsmVLlTc8TZo0SWbNmiUrV64MfXvFihVSs2ZN2bp1q2o7NRMlnJp0WbffCFDCfouYN/ubkIT37dsXEkz0cCBMSLhdu3aCX9affPKJtG7dWv744w/JkSOHkrhTHlKGmNq0aSM7d+6UEydOCAS6fPlyKVeunPTp00d++ukngeA//fRTefXVV9X/UU/jxo2levXq0r9//5gShjAhMAgQZV566SXp1q2b9OzZUysC0RLGDcFXX32lhJs5c2Y5c+aMuslA3x5//HHV96pVq6obEfQTNwujR4+WEiVKyI8//igvvPCC4tKvX78k+4ObjPfff1/WrFkTkQf1Pf/884obEm5K0NahQ4cEMqaEtULMQiSgRYAS1sLGQlEEXJHwxYsXJVOmTLJ//37Jnz9/qAkItlixYnL16tUICTsZMOMdPHiw3HXXXWo2Wb9+ffXRDz/8IE8//bT8/vvvsm7dOrlw4YJUq1ZNfYaZ5/r169XMOTpB5I6Enc8gp0aNGsnBgweVKBcvXqxEiTrRt6lTpyqZot+4YciTJ48qihk0vl+nTp3QTHjGjBlq9o6Z6C233KLyjRo1SvW9ZMmSMmDAAClYsKCMHDlSzcbRl927d0uWLFlCXd20aZNqB31KKp06dUr1B3mLFy+usu3du1fN7CHc7Nmzq+9h5QBscaOCmx5KmD/fJGCOACVsjrXNLbkiYQB64okn5MCBA2rWWatWrQgZO8vR0TPpO++8U0mtQYMGIcaYwaIOiGvcuHHXsG/YsKFqCzPueCSMPPny5ZMPP/xQzbLLli2rJAqhYYa6evVqtayMPkD8mKUiYTkd38cyMmQ6ceJEwTI0yhYuXDjUtCNhLEejTvyL2Wn37t3VDB8zWp3UokULKV26dGjGP2TIECV75+Zjx44d0rRpUzW7rly5MiWsA5llSCAFBCjhFMBj0RCBhCTs7Ok6pbH36eyVYjaMPcuPP/5YzWQxK8MyMkQRLWHkRT4sFaN8rly5VJVYMobUsCe6YMECufXWWyNCNWjQICXBr7/+Ws0045Xw3XffrWR2+vRpJeMvv/xSFf3nn39U2+fOnZPp06fLokWL1DI5ZtoQMvqGZWeMIW/evEqqaD98xpmUhLHsjDLDhw/Xuty++eYb6dy5s0C2SLh5ePfdd6V27drqa/wLvtgPhvw5E9bCzEIkoE2AEtZGx4JhBBKS8PX2hMOpnj17Vs0iO3TooPZlMVPDnrAjcRyawqz0zTffVPum4Ql7rDiE9cEHH8iGDRvU/jKWXLE8jOVtyCZr1qxqz7hevXohIX300UdqXzl6ORoZMBOGXLHHiiXc3Llzh5rEzBuHmtA/LP0ePnxYHSjDni5mn5Bws2bN1IGz+fPny/jx49XhL+dgWFIS7tWrl1oCx0EqnXTlyhU1G8fNCng1adJE9uzZo24+sCz+3XffybRp01TVlLAOYZYhgZQRoIRTxo+l/4+AKxKGnHE6OnxZGZVjjxcHqXCQKPxgVjR8CO7YsWNqGRsJAsqQIYPaY77tttvUgS8sdWMpFvUgYTaNMkiQO2a0sSSMGSUkeuTIEYGoIXHINFbCSW4shffu3VvJs2LFikrC4aejURduCubOnauqSErC2HvGATHsCTszfeSH8OfMmaNOfCeXcHjr33//lXTp0qn9aacMmK5atSp0Wh0ccubMqZa+r7fXnFx7yX3O09HJEeLnQSJACQcp2qk3VlckvH37dqlSpYqaoeGEMH5ZY9kWsli2bJmUL1/+uhJesmSJEvX333+vDkuhnldeeUXNJFEPlq2xRAwZXS+FSxiSxiwcy8KY/bZv316JGPvQ2AfGQSrsp0LsY8eOVdViOR0nn7ds2SIYE1K0hLGkjZk9Zvm4OQiXMJa9hw0bpm4+cCOBmwocAHvnnXfUcjJm79hzxqweY0ouYeaPGwPckHzxxRfqhHWsxJlwciT5OQm4T4ASdp9pEGtMSMKxnhPG6WY8C4sZJ2SHw0OQMGSK7+MQVVIHs8KB40Qx9jxxMhiyGTNmjDoR3apVK5k9e3aEgCF1CC06hT8njGVbHGzCsnDz5s1DWZ3T0Vj2xiljCBLlkPDsb4ECBdRM2Jl1xnpOGKeWUWbhwoVK5M5zwmCBw15vvPGGkixmsdi3xewbzz9D/BA3xhRvQjvOTU1SZSjheGkyHwm4R4ASdo9lkGuKS8JBBsSx/48Al6N5NZBA2M8DYZCACwQoYRcgBqUKSjgokeY44yHAmXA8lJgnOQKUcHKE+HmIACXMi4EEOBPmNeAuAUrYXZ5W10YJWx1eDi5BApwJJwiM2WMSoIR5YcRNgBKOGxUzBoAAJRyAIBsYIiVsALItTVDCtkSS43CDACXsBkXWQQnzGoibACUcNypmDAABSjgAQTYwRErYAGRbmqCEbYkkx+EGAUrYDYqs4yoRkECCBPi7J0FgzG4nAf4g2BlXjooESIAESMAHBChhHwSJXSQBEiABErCTACVsZ1w5KhIgARIgAR8QoIR9ECR2kQRIgARIwE4ClLCdceWoSIAESIAEfEDgP9+BkAjAjGoZAAAAAElFTkSuQmCC" style="cursor:pointer;max-width:100%;" onclick="(function(img){if(img.wnd!=null&&!img.wnd.closed){img.wnd.focus();}else{var r=function(evt){if(evt.data=='ready'&&evt.source==img.wnd){img.wnd.postMessage(decodeURIComponent(img.getAttribute('src')),'*');window.removeEventListener('message',r);}};window.addEventListener('message',r);img.wnd=window.open('https://viewer.diagrams.net/?client=1&page=0&edit=_blank');}})(this);"
    />
</div>




### Overview ESP32

The ESP32 is used as a based

[Espressif IDF Framework](https://idf.espressif.com/)
<!--Leave any add-ons/plugins for the acknowledgements section.-->
<!--
* [![Next][Next.js]][Next-url]
* [![React][React.js]][React-url]
* [![Vue][Vue.js]][Vue-url]
* [![Angular][Angular.io]][Angular-url]
* [![Svelte][Svelte.dev]][Svelte-url]
* [![Laravel][Laravel.com]][Laravel-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![JQuery][JQuery.com]][JQuery-url]
-->
<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This project is based on [ESP32 IDF documentation](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/index.html).
 
In order to compile and flash your board we will use a docker image, espressif alredy provides their [own official image](https://hub.docker.com/r/espressif/idf). Find specific documentation and usage [here](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/tools/idf-docker-image.html).

To get a local copy up and running follow these simple example steps.

### Prerequisites

List of things you need to use the software and how to install them.

* [docker](https://docs.docker.com/engine/install/)
  <!--```sh
  npm install npm@latest -g
  ```-->
* 5 GB for docker image
* [ESP32-DevKitC V4](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/hw-reference/esp32/get-started-devkitc.html#get-started-esp32-devkitc-board-front) or equivalent

### Installation

_Below is an example of how you can install and set up the development environment._

1. Get stable image [espressif/idf](https://hub.docker.com/r/espressif/idf)

   ```
   $ sudo docker pull espressif/idf
    [sudo] password for <user>: 
    Using default tag: latest
    latest: Pulling from espressif/idf
    49b384cc7b4a: Pull complete 
    f06a9c6dea87: Pull complete 
    3248dfbeb8a9: Pull complete 
    7bb60fb3c508: Pull complete 
    eb4c6a2a5996: Pull complete 
    79396d0c2f1d: Pull complete 
    f15d38b02862: Pull complete 
    4f4fb700ef54: Pull complete 
    Digest: sha256:128f01e878e93f56e33f9161c27994de984744ddb784a275be93165351798233
    Status: Downloaded newer image for espressif/idf:latest
    docker.io/espressif/idf:latest
   $ sudo docker images | grep espressif
    espressif/idf               latest    548f67c360eb   26 hours ago    4.83GB
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

_The main usage of the previous downloaded docker image is to build the source code and flash it._

Further documentation how to use espressif docker image can be found [here](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/tools/idf-docker-image.html)

The makefile on root folder from ESP32 does the job for us:


1. Set environment variables under env.json file.
   ```
   {
    "wifi_ssid":"<Insert your Wifi SSID here>",
    "wifi_password":"<Insert your Wifi Passowrd here>",
    "mqtt_server":"192.168.XX.XXX"
    }
   ```

### CI - Background  Mode

2. Build image from source project folder, run command under ESP32 directory.
   ```
   make build_linux
   ```

It will generate a build folder with all the object files under  iot_fraemwork_source_code directory.

3. Plug Device via USB and ensure is recognized _(only for Linux OS or  MAC OS - adapt of makefile command can be required as now it is pointing to /device/ttyUSB0)_
4. Flash image from source project folder, run command under ESP32 directory. _(only for Linux OS or  MAC OS)_
   ```
   make flash_linux
   ```

It will start flashing the device and reseting it. No button press or other action required.

### Interactive Mode

2. Run container and attach to it.
   ```
   make run_espimage_linux_it
   ```
This will provide access to the container image and build/flash/monitor commands from idf tool can be  use as usual, please refer to more info for this commands [here](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/tools/idf-py.html)

Basic usage : Under /project  folder

Start the Graphical Configuration Tool
   ```
   idf.py menuconfig
   ```
Build
   ```
   idf.py build
   ```
Clean
   ```
   idf.py clean
   ```
Flash
   ```
   idf.py flash
   ```
Monitor
   ```
   idf.py monitor
   ```
   _Note to exit the monitor use Ctrl+X+T or terminate the terminal_

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### GIT Management for sdkconfig/env.json or other files private files

As sdkconfig is a file that is usually changed for IPs or data that is private as passwords or WIFI SSIDs, that data does not want to be included. 

For this those are excluded under the git repo to avoid mistakes.

```
  git update-index --no-assume-unchanged pathTo/fileToExclude # include again to check changes
  git update-index --assume-unchanged pathTo/fileToExclude  # ignore further changes to a file
```

<!-- ROADMAP -->
## Roadmap

- [x] Add Readme
- [x] Include Development Docker Image setup
- [x] Add Base Example of MQTT
- [ ] Sensor 
    - [ ] BME280 Integration via I2C
    - [ ] BME680 Integration via I2C
    - [ ] Generic I2C Device controller
- [ ] Actuators
    - [ ] Relays controller
    - [ ] Motor Board controller IBT_2
    - [ ] Generic PWM controller
- [ ] BLE
    - [ ] BLE example setup - initial wifi configuration
    - [ ] BLE mesh and sensors data receiver


See the [open issues](https://gitlab.com/Kaervek/iot_greenhouse_framework/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTRIBUTING 
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>
-->


<!-- LICENSE -->
## License

As an open source project it  applies the GNU General Public License (GPL)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

[Kaervek](https://www.reddit.com/user/NoSea1819/) - Reddit user - chat here or via email hugomallo@hotmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS 
## Acknowledgments

Use this space to list resources you find helpful and would like to give credit to. I've included a few of my favorites to kick things off!

* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Malven's Flexbox Cheatsheet](https://flexbox.malven.co/)
* [Malven's Grid Cheatsheet](https://grid.malven.co/)
* [Img Shields](https://shields.io)
* [GitHub Pages](https://pages.github.com)
* [Font Awesome](https://fontawesome.com)
* [React Icons](https://react-icons.github.io/react-icons/search)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

-->

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links 
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
[Angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[Angular-url]: https://angular.io/
[Svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[Svelte-url]: https://svelte.dev/
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 
-->