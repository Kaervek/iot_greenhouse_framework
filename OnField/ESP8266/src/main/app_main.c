/* MQTT over Websockets Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/timers.h"
#include "freertos/private/projdefs.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "driver/i2c.h"
#include "bme280.h"

/******************************************************************************/
/*!                       Macros                                              */

#define SAMPLE_COUNT  UINT8_C(50)
int8_t *interfacePointerGLobal;
int8_t * interfacePointerResult;

int8_t rslt;
    uint32_t period;
    struct bme280_dev dev;
    struct bme280_settings settings;

#define I2C_EXAMPLE_MASTER_SCL_IO           5                /*!< gpio number for I2C master clock */
#define I2C_EXAMPLE_MASTER_SDA_IO           4               /*!< gpio number for I2C master data  */
#define I2C_EXAMPLE_MASTER_NUM              I2C_NUM_0        /*!< I2C port number for master dev */
#define I2C_EXAMPLE_MASTER_TX_BUF_DISABLE   0                /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_RX_BUF_DISABLE   0                /*!< I2C master do not need buffer */


#define WRITE_BIT                           I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT                            I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN                        0x1              /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS                       0x0              /*!< I2C master will not check ack from slave */
#define ACK_VAL                             0x0              /*!< I2C ack value */
#define NACK_VAL                            0x1              /*!< I2C nack value */
#define LAST_NACK_VAL                       0x2              /*!< I2C last_nack value */

static const char *TAG = "MQTTWS_EXAMPLE";
esp_mqtt_client_handle_t clientGlobal ;



// C program for implementation of ftoa() 
#include <math.h> 
#include <stdio.h> 
 
// Reverses a string 'str' of length 'len' 
static void reverse(char* str, int len) 
{ 
    int i = 0, j = len - 1, temp; 
    while (i < j) { 
        temp = str[i]; 
        str[i] = str[j]; 
        str[j] = temp; 
        i++; 
        j--; 
    } 
} 
 
// Converts a given integer x to string str[]. 
// d is the number of digits required in the output. 
// If d is more than the number of digits in x, 
// then 0s are added at the beginning. 
static int intToStr(int x, char str[], int d) 
{ 
    int i = 0; 
    while (x) { 
        str[i++] = (x % 10) + '0'; 
        x = x / 10; 
    } 
 
    // If number of digits required is more, then 
    // add 0s at the beginning 
    while (i < d) 
        str[i++] = '0'; 
 
    reverse(str, i); 
    str[i] = '\0'; 
    return i; 
} 
 
// Converts a floating-point/double number to a string. 
static void ftoa(float n, char* res, int afterpoint) 
{ 
    // Extract integer part 
    int ipart = (int)n; 
 
    // Extract floating part 
    float fpart = n - (float)ipart; 
 
    // convert integer part to string 
    int i = intToStr(ipart, res, 0); 
 
    // check for display option after point 
    if (afterpoint != 0) { 
        res[i] = '.'; // add dot 
 
        // Get the value of fraction part upto given no. 
        // of points after dot. The third parameter 
        // is needed to handle cases like 233.007 
        fpart = fpart * pow(10, afterpoint); 
 
        intToStr((int)fpart, res + i + 1, afterpoint); 
    } 
} 
 



/******************************************************************************/
/*!         Static Function Declaration                                       */

/*!
 *  @brief This internal API is used to get compensated temperature data.
 *
 *  @param[in] period   : Contains the delay in microseconds.
 *  @param[in] dev      : Structure instance of bme280_dev.
 *
 *  @return Status of execution.
 */
static struct bme280_data get_tempHumPress(uint32_t period, struct bme280_dev *dev);

/**
 * @brief i2c master initialization
 */
static esp_err_t i2c_master_init()
{
    int i2c_master_port = I2C_EXAMPLE_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_EXAMPLE_MASTER_SDA_IO;
    conf.sda_pullup_en = 1;
    conf.scl_io_num = I2C_EXAMPLE_MASTER_SCL_IO;
    conf.scl_pullup_en = 1;
    conf.clk_stretch_tick = 300; // 300 ticks, Clock stretch is about 210us, you can make changes according to the actual situation.
    ESP_ERROR_CHECK(i2c_driver_install(i2c_master_port, conf.mode));
    ESP_ERROR_CHECK(i2c_param_config(i2c_master_port, &conf));
    return ESP_OK;
}

/**
 * @brief test code to write i2c bme280
 *
 * 1. send data
 * ___________________________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | write reg_address + ack | write data_len byte + ack  | stop |
 * --------|---------------------------|-------------------------|----------------------------|------|
 *
 * @param i2c_num I2C port number
 * @param reg_address slave reg address
 * @param data data to send
 * @param data_len data length
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Sending command error, slave doesn't ACK the transfer.
 *     - ESP_ERR_INVALID_STATE I2C driver not installed or not in master mode.
 *     - ESP_ERR_TIMEOUT Operation timeout because the bus is busy.
 */


void BME280_delay_msek(uint32_t msek)
{
	vTaskDelay(msek/portTICK_PERIOD_MS);
}

static int8_t i2c_master_bme280_write(uint8_t reg_address, const uint8_t *data, size_t data_len, void *intf_ptr)
{
    int ret;
    uint8_t dev_id = *((uint8_t *) intf_ptr); // TODOKAERVEK
    
    //ESP_LOGI("I2C", "Write Request");
    
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd,  (BME280_I2C_ADDR_PRIM << 1)  | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_address, ACK_CHECK_EN);
    i2c_master_write(cmd, (uint8_t *) data, data_len, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_EXAMPLE_MASTER_NUM, cmd, pdMS_TO_TICKS(1000));
    i2c_cmd_link_delete(cmd);
    
    return ret;
}

/**
 * @brief test code to read i2c bme280
 *
 * 1. send reg address
 * ______________________________________________________________________
 * | start | slave_addr + wr_bit + ack | write reg_address + ack | stop |
 * --------|---------------------------|-------------------------|------|
 *
 * 2. read data
 * ___________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | read data_len byte + ack(last nack)  | stop |
 * --------|---------------------------|--------------------------------------|------|
 *
 * @param i2c_num I2C port number
 * @param reg_address slave reg address
 * @param data data to read
 * @param data_len data length
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Sending command error, slave doesn't ACK the transfer.
 *     - ESP_ERR_INVALID_STATE I2C driver not installed or not in master mode.
 *     - ESP_ERR_TIMEOUT Operation timeout because the bus is busy.
 */
static int8_t i2c_master_bme280_read(uint8_t reg_address, uint8_t *data, size_t data_len, void *intf_ptr)
{
    int ret;
    uint8_t dev_id = *((uint8_t *) intf_ptr); // TODOKAERVEK
    
    //ESP_LOGI("I2C", "Read Request");

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (BME280_I2C_ADDR_PRIM << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_address, ACK_CHECK_EN);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (BME280_I2C_ADDR_PRIM << 1) | READ_BIT, ACK_CHECK_EN);

    if (data_len > 1) {
		i2c_master_read(cmd, data, data_len - 1, I2C_MASTER_ACK);
	}
    i2c_master_read_byte(cmd, data+data_len-1, I2C_MASTER_NACK);

    i2c_master_stop(cmd);


    ret = i2c_master_cmd_begin(I2C_EXAMPLE_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    return ret;
}

static void i2c_task_example (void *arg) {
    ESP_LOGI(TAG, "Task i2c_task_example reached");
    int8_t rslt;
    get_tempHumPress(period, &dev);
    /* De-initialize the i2c driver*/
    //i2c_driver_delete(I2C_EXAMPLE_MASTER_NUM);
    vTaskDelete(NULL);
}

static void vTimerCallback(TimerHandle_t pxTimer)
{    
    ESP_LOGI(TAG, "Timer callback reached");
    xTaskCreate(i2c_task_example, "i2c_task_example", 2048, NULL, 1, NULL);

    /*
        uint32_t value = (rand() % ( 50 - -20 + 1)) + -20; 
        char int_str[20];
        sprintf(int_str, "%d", value);
        esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/temperature", int_str, 0, 0, 0);
        value = (rand() % ( 100 - 0 + 1)) + 0; 
        
        sprintf(int_str, "%d", value);
        esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/humidity", int_str, 0, 0, 0);
        value = (rand() % ( 1600 - 400 + 1)) + 400; 
        
        sprintf(int_str, "%d", value);
        esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/pressure", int_str, 0, 0, 0);
    */

    
}

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    clientGlobal = event->client;
    int msg_id;
    // your_context_t *context = event->context;
    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
        msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
        ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

        msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", 1);
        ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

        msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
        ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
        ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG, "MQTT_EVENT_DATA");
        printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
        printf("DATA=%.*s\r\n", event->data_len, event->data);
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        break;
    default:
        ESP_LOGI(TAG, "Other event id:%d", event->event_id);
        break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_app_start(void)
{
    const esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_URI,
    };

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);

    /* CREATE TIMER 10 SECONDS */
    uint32_t timerId = 1;
    TimerHandle_t xTimers = xTimerCreate("Timer",               // Just a text name, not used by the kernel.
                                         (pdMS_TO_TICKS(10000)), // The timer period in ticks.
                                         pdTRUE,                // The timers will auto-reload themselves when they expire.
                                         (void *)timerId,       // Assign each timer a unique id equal to its array index.
                                         vTimerCallback         // Each timer calls the same callback when it expires.
    );
    if (xTimers == NULL)
    { // The timer was not created.
    }
    else

    {
        // Start the timer.  No block time is specified, and even if one was
        // it would be ignored because the scheduler has not yet been
        // started.
        if (xTimerStart(xTimers, 0) != pdPASS)
        {
            // The timer could not be set into the Active state.
        }
    }
}

void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_WS", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    mqtt_app_start();
    
    /* Initialize the I2C */

    ESP_ERROR_CHECK(i2c_master_init(I2C_EXAMPLE_MASTER_NUM));

    

    /* Interface selection is to be updated as parameter
     * For I2C :  BME280_I2C_INTF
     * For SPI :  BME280_SPI_INTF
     */
    /*! Chip Id */
    dev.chip_id = BME280_CHIP_ID;
    /*! Interface Selection
     * For SPI, intf = BME280_SPI_INTF
     * For I2C, intf = BME280_I2C_INTF
     */
    dev.intf= BME280_I2C_INTF;

    /*!
     * The interface pointer is used to enable the user
     * to link their interface descriptors for reference during the
     * implementation of the read and write interfaces to the
     * hardware.
     */
    dev.intf_ptr = interfacePointerGLobal;

    /*! Variable to store result of read/write function */
    dev.intf_rslt = interfacePointerResult;

    /*! Read function pointer */
    dev.read = i2c_master_bme280_read;

    /*! Write function pointer */
    dev.write = i2c_master_bme280_write;

    /*! Delay function pointer */
    dev.delay_us = BME280_delay_msek;
    /*! Trim data */
    //struct bme280_calib_data calib_data;

    /*ESP_LOGI("I2C","Calib Data T1 [%d]",dev.calib_data.dig_t1);
    ESP_LOGI("I2C","Calib Data T2 [%d]",dev.calib_data.dig_t2);
    ESP_LOGI("I2C","Calib Data T3 [%d]",dev.calib_data.dig_t3);
*/
    rslt = bme280_init(&dev);

    /*ESP_LOGI("I2C","Calib Data T1 [%d]",dev.calib_data.dig_t1);
    ESP_LOGI("I2C","Calib Data T2 [%d]",dev.calib_data.dig_t2);
    ESP_LOGI("I2C","Calib Data T3 [%d]",dev.calib_data.dig_t3);
*/

    /* Always read the current settings before writing, especially when all the configuration is not modified */
    rslt = bme280_get_sensor_settings(&settings, &dev);

    /* Configuring the over-sampling rate, filter coefficient and standby time */
    /* Overwrite the desired settings */
    settings.filter = BME280_FILTER_COEFF_2;

    /* Over-sampling rate for humidity, temperature and pressure */
    settings.osr_h = BME280_OVERSAMPLING_1X;
    settings.osr_p = BME280_OVERSAMPLING_1X;
    settings.osr_t = BME280_OVERSAMPLING_1X;

    // /* Setting the standby time */
    settings.standby_time = BME280_STANDBY_TIME_0_5_MS;

    rslt = bme280_set_sensor_settings(BME280_SEL_ALL_SETTINGS, &settings, &dev);

    // /* Always set the power mode after setting the configuration */
    rslt = bme280_set_sensor_mode(BME280_POWERMODE_NORMAL, &dev);

    // /* Calculate measurement time in microseconds */
    rslt = bme280_cal_meas_delay(&period, &settings);

    /*ESP_LOGI("I2C","Calib Data T1 [%d]",dev.calib_data.dig_t1);
    ESP_LOGI("I2C","Calib Data T2 [%d]",dev.calib_data.dig_t2);
    ESP_LOGI("I2C","Calib Data T3 [%d]",dev.calib_data.dig_t3);
    */
    /* CREATE TIMER 10 SECONDS */
    uint32_t timerId = 1;
    TimerHandle_t xTimers = xTimerCreate("Timer",               // Just a text name, not used by the kernel.
                                         (pdMS_TO_TICKS(5000)), // The timer period in ticks.
                                         pdTRUE,                // The timers will auto-reload themselves when they expire.
                                         (void *)timerId,       // Assign each timer a unique id equal to its array index.
                                         vTimerCallback         // Each timer calls the same callback when it expires.
    );
    if (xTimers == NULL)
    { // The timer was not created.
    }
    else

    {
        // Start the timer.  No block time is specified, and even if one was
        // it would be ignored because the scheduler has not yet been
        // started.
        if (xTimerStart(xTimers, 0) != pdPASS)
        {
            // The timer could not be set into the Active state.
        }
    }

}


/*!
 *  @brief This internal API is used to get compensated temperature data.
 */
static struct bme280_data get_tempHumPress(uint32_t period, struct bme280_dev *dev)
{
    int8_t rslt = BME280_E_NULL_PTR;
    int8_t idx = 0;
    uint8_t status_reg;
    struct bme280_data comp_data;

    while (idx < 1) //SAMPLE_COUNT
    {
        rslt = bme280_get_regs(BME280_REG_STATUS, &status_reg, 1, dev);
        //bme280_error_codes_print_result("bme280_get_regs", rslt);
        if (status_reg & BME280_STATUS_MEAS_DONE)
        {

            /* Measurement time delay given to read sample */
            dev->delay_us(period, dev->intf_ptr);

            /* Read compensated data */
            rslt = bme280_get_sensor_data(BME280_PRESS | BME280_TEMP | BME280_HUM, &comp_data, dev);
            char res[20]; 
            //bme280_error_codes_print_result("bme280_get_sensor_data", rslt);
            ESP_LOGI("I2C","-----------------------");
            ESP_LOGI("I2C","Temperature");
            ftoa(comp_data.temperature, res, 4); 
            ESP_LOGI("I2C","A %s deg C\n", res);
            esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/temperature", res, 0, 0, 0);
            ESP_LOGI("I2C","-----------------------");
            ESP_LOGI("I2C","Humidity");
            ftoa(comp_data.humidity, res, 4);
            ESP_LOGI("I2C","A %s porciento \n", res);
            esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/humidity",res, 0, 0, 0);
            ESP_LOGI("I2C","-----------------------");
            ESP_LOGI("I2C","Pressure");
            ftoa(comp_data.pressure/100, res, 4);
            ESP_LOGI("I2C","A %s Pa\n", res);
            esp_mqtt_client_publish(clientGlobal, "esp8266/bme280/pressure", res, 0, 0, 0);
            idx++;
        }
        idx++;
    }

    return comp_data;
}