Xtensa toolchain is built, to use it:

export PATH=/usr/home/esp-open-sdk/xtensa-lx106-elf/bin:$PATH

Espressif ESP8266 SDK is installed. Toolchain contains only Open Source components
To link external proprietary libraries add:

xtensa-lx106-elf-gcc -I/usr/home/esp-open-sdk/sdk/include -L/usr/home/esp-open-sdk/sdk/lib

RTOS SDK currently used

git clone --recursive https://github.com/espressif/ESP8266_RTOS_SDK.git