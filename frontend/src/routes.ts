import { Route } from '@angular/router'

export const appRoutes: Route[] = [

    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', loadComponent: () => import('./app/login/login.component').then(mod => mod.LoginComponent)},
    {path: 'home', loadComponent: () => import('./app/dashboard/dashboard.component').then(mod => mod.DashboardComponent)},
    {path: '**', redirectTo: 'login', pathMatch: 'full'}
];