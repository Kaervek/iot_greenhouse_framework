import { AppComponent } from './app/app.component';
import { bootstrapApplication, provideProtractorTestingSupport } from '@angular/platform-browser';
import { provideRouter } from '@angular/router'
import { appRoutes } from './routes';
// Environment Variables
import { environment as env } from 'src/environments/environment.dev';
// MQTT Libraries
import { IMqttServiceOptions, MqttModule } from 'ngx-mqtt';
import { importProvidersFrom } from '@angular/core';

const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: env.mqtt.server,
  port: env.mqtt.port,
  protocol: (env.mqtt.protocol == "wss") ? "wss" : "ws",
  path: '/mqtt',
  clientId: env.mqtt.clientId
};

bootstrapApplication(AppComponent,
  {
    providers: [
      provideProtractorTestingSupport(),
      provideRouter(appRoutes),
      importProvidersFrom(MqttModule.forRoot(MQTT_SERVICE_OPTIONS))
    ]
    
  
  }).catch(err => console.error(err));
