import { Injectable } from "@angular/core";
import { IMqttMessage, MqttService } from "ngx-mqtt";
import { Observable } from "rxjs";

@Injectable()
export class Esp8266Service {
    private endpoint: string;
    

    constructor(
        private _mqttService: MqttService,
    ) {
        this.endpoint = 'esp8266';
    }

    topic(sensorID: string, eventName : string) : Observable <IMqttMessage> {
        let topicName =`${this.endpoint}/${sensorID}/${eventName}`; 
        return this._mqttService.observe(topicName);
    }

}