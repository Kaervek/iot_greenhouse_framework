import { Component, Inject, NgZone, PLATFORM_ID , Input, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { isPlatformBrowser, CommonModule } from '@angular/common';

// amCharts imports
import * as am5 from '@amcharts/amcharts5';
import * as am5xy from '@amcharts/amcharts5/xy';
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
// TODO-KAERVEK CREATE MODEL TO PASS DATA REQUIRED TO BUILD GAUGE in order to generate generic gauges (interesting also configuration of bands)
@Component({
  selector: 'historygram-chart',
  template: `
  <!--div>
    <button class="btn">View All</button>
    <button class="btn">Last Month</button>
    <button class="btn">Last Week</button>
    <button class="btn">Last 24h</button>
  </div-->
  <div class="scroll-cards">
    <div class="mail-info">{{historyData['type']}}</div>
    <div id="historygram" class="card"></div>
  </div>  
  `,
  styleUrls: ['./historygram.component.css'],
  standalone: true,
  imports: [CommonModule]
})

export class HistorygramChartComponent implements OnChanges , DoCheck{
  private mainRoot: am5.Root;
  //private series : am5.Chart<any>;
  private series : any;
  
  @Input() historyData : any = { 'type' : 'Temperature', 'events': [{    "time": Date().toLocaleString(),    "value": 0  }]}; // decorate the propertz with @Input() 
  private originalLength : any = 0;
  private oldType : string = "Temperature";
  constructor(@Inject(PLATFORM_ID) private platformId: Object, private zone: NgZone) {

  }
 
  

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      let root = am5.Root.new("historygram");

      
    // Set themes
    // https://www.amcharts.com/docs/v5/concepts/themes/
    root.setThemes([
      am5themes_Animated.new(root)
    ]);  


    // Create chart
    // https://www.amcharts.com/docs/v5/charts/xy-chart/
    let chart = root.container.children.push(am5xy.XYChart.new(root, {
      focusable: true,
      panX: true,
      panY: true,
      wheelX: "panX",
      wheelY: "zoomX",
      pinchZoomX:true,
      paddingLeft: 0
    }));

    let easing = am5.ease.linear;


    // Create axes
    // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
    let xAxis = chart.xAxes.push(am5xy.DateAxis.new(root, {
      maxDeviation: 0.1,
      groupData: false,
      baseInterval: {
        timeUnit: "second",
        count: 10
      },
      renderer: am5xy.AxisRendererX.new(root, {
        //minorGridEnabled: true,
        minGridDistance: 70
      }),
      tooltip: am5.Tooltip.new(root, {})
    }));

    let yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
      maxDeviation: 0.2,
      renderer: am5xy.AxisRendererY.new(root, {})
    }));


    // Add series
    // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
    this.series = chart.series.push(am5xy.LineSeries.new(root, {
      minBulletDistance: 10,
      connect: false,
      xAxis: xAxis,
      yAxis: yAxis,
      valueYField: "value",
      valueXField: "time",
      tooltip: am5.Tooltip.new(root, {
        pointerOrientation: "horizontal",
        labelText: "{valueY}"
      })
    }));

    this.series.fills.template.setAll({
      fillOpacity: 0.2,
      visible: true
    });

    this.series.strokes.template.setAll({
      strokeWidth: 2
    });


    // Set up data processor to parse string dates
    // https://www.amcharts.com/docs/v5/concepts/data/#Pre_processing_data
    this.series.data.processor = am5.DataProcessor.new(root, {
      dateFormat: "M/d/yyyy h:mm:ss am/pm",
      dateFields: ["time"]
    });
    this.series.data.setAll(this.historyData['events']);
   
    chart.set("cursor", am5xy.XYCursor.new(root, {
      behavior: "zoomX"
    }));

    xAxis.set("tooltip", am5.Tooltip.new(root, {
      themeTags: ["axis"]
    }));
    
    yAxis.set("tooltip", am5.Tooltip.new(root, {
      themeTags: ["axis"]
    }));

    this.mainRoot = root;
  
  });

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("NgONchanges");
    if(!changes['historyData'].firstChange) {
      
    /*this.dataItem.animate({
        key: "value",
        to: this.historyData,
        duration: 2000,
        easing: am5.ease.out(am5.ease.cubic)
      });*/
      
    }
    
  
}

  ngDoCheck(): void {
    
    if (this.historyData['events'].length != this.originalLength || this.historyData['type'] != this.oldType) {

       this.originalLength = this.historyData['events'].length;
       this.series.data.setAll(this.historyData['events']);
       this.oldType = this.historyData['type'];

    }
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.mainRoot) {
        this.mainRoot.dispose();
      }
    });
  }
 
  
}