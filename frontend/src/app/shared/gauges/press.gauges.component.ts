import { Component, Inject, NgZone, PLATFORM_ID , Input, OnChanges, SimpleChanges } from '@angular/core';
import { isPlatformBrowser, CommonModule } from '@angular/common';

// amCharts imports
import * as am5 from '@amcharts/amcharts5';
import * as am5xy from '@amcharts/amcharts5/xy';
import * as am5radar from '@amcharts/amcharts5/radar';

// TODO-KAERVEK CREATE MODEL TO PASS DATA REQUIRED TO BUILD GAUGE in order to generate generic gauges (interesting also configuration of bands)
@Component({
  selector: 'press-gauge-chart',
  template: '<div id="chartpress" style="width: 300px; height: 250px"></div>',
  standalone: true,
  imports: [CommonModule]
})

export class PressGaugesChartComponent implements OnChanges {
  private mainRoot: am5.Root;
  private dataItem : am5.DataItem<any>;
  
  @Input() handClockData = 25; // decorate the propertz with @Input() 

  constructor(@Inject(PLATFORM_ID) private platformId: Object, private zone: NgZone) {

  }
 
  

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      let root = am5.Root.new("chartpress");

      let chart = root.container.children.push(
        am5radar.RadarChart.new(root, {})
      );

      // Creage Gauge Axis
      let axis = chart.xAxes.push(
        am5xy.ValueAxis.new(root, {
            min: 500,
            max: 1500,
            renderer: am5radar.AxisRendererCircular.new(root, {
                startAngle: -180,
                endAngle: 0,
                strokeOpacity: 0.1,
                minGridDistance: 30
            })
        })

        
      );
        
    // Create clock hands
      let handDataItem = axis.makeDataItem({
        value: 0
      });
      let hand = handDataItem.set("bullet", am5xy.AxisBullet.new(root, {
        sprite: am5radar.ClockHand.new(root, {
          radius: am5.percent(99)
        })
      }));
      
      axis.createAxisRange(handDataItem);
      
      handDataItem.animate({
        key: "value",
        to: this.handClockData,
        duration: 2000,
        easing: am5.ease.out(am5.ease.cubic)
      });
      this.dataItem = handDataItem;
      
      this.mainRoot = root;
    });

  }

  ngOnChanges(changes: SimpleChanges): void {
    if(!changes['handClockData'].firstChange) {
    this.dataItem.animate({
        key: "value",
        to: this.handClockData,
        duration: 2000,
        easing: am5.ease.out(am5.ease.cubic)
      });
    }
    
        
    
    

  
}

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.mainRoot) {
        this.mainRoot.dispose();
      }
    });
  }
 
  
}