import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';


// ROUTES
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// SERVICES 
import { Esp8266Service } from './services/backend/MQTT_API/esp8266/esp8266.mqtt.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  standalone: true ,
  providers: [Esp8266Service],
  imports: [
    CommonModule, 
    RouterOutlet,
    
    LoginComponent,
    DashboardComponent]
})
export class AppComponent {
  
}
