import { Component } from '@angular/core';
import { DashSideboardComponent } from './dashSideboard/dashSideboard.component';
import { DashMainBoardComponent } from './dashMainboard/dashMainboard.component';

@Component({
  selector: 'dashboard-home',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  standalone: true,
  imports : [DashSideboardComponent, DashMainBoardComponent]
})
export class DashboardComponent {
  pageTitle = "Dashboard";

}
