import { Component , OnInit , OnDestroy} from '@angular/core';
import { CommonModule } from '@angular/common';
// ADD MQTT SERVICE
import { Subscription } from 'rxjs';
import { Esp8266Service } from 'src/app/services/backend/MQTT_API/esp8266/esp8266.mqtt.service';
import { IMqttMessage } from 'ngx-mqtt';
import { TempGaugesChartComponent } from 'src/app/shared/gauges/temp.gauges.component';
import { PressGaugesChartComponent } from 'src/app/shared/gauges/press.gauges.component';
import { HumGaugesChartComponent } from 'src/app/shared/gauges/humidity.gauges.component';
import { HistorygramChartComponent } from 'src/app/shared/historygram/historygram.component';


@Component({
  selector: 'dashMainboard',
  templateUrl: './dashMainboard.component.html',
  styleUrls: ['./dashMainboard.component.css'],
  standalone: true,
  imports: [CommonModule, PressGaugesChartComponent,TempGaugesChartComponent, HumGaugesChartComponent, HistorygramChartComponent]
})
export class DashMainBoardComponent implements OnInit , OnDestroy{
    private now: Date = new Date();
    esp8266_events_selected: any = { 'type': 'Temperature' , 'events' :[{'time': Date().toLocaleString(), 'value': 0 }]};
    //esp8266_events_temperature: any = 0;
    esp8266_events_temperature: any[] = [{'time': Date().toLocaleString(), 'value': 0 }];
    //esp8266_events_pressure: any = 0;
    esp8266_events_pressure: any[] = [{'time': Date().toLocaleString(), 'value': 0 }];
    //esp8266_events_humidity: any = 0;
    esp8266_events_humidity: any[] = [{'time': Date().toLocaleString(), 'value': 0 }];
    private sensorID: string;
    private eventNameTemp: string;
    private eventNamePress: string;
    private eventNameHumidity: string;
    subscription: Subscription;
    subscription2: Subscription;
    subscription3: Subscription;

    constructor(
        private readonly _esp8266service: Esp8266Service
    ) {
        // Here could load all the MQTT topics that shall be subscribed by default
        this.sensorID ="bme280";
        this.eventNameTemp = "temperature";
        this.eventNamePress = "pressure";
        this.eventNameHumidity = "humidity";
    }

    ngOnInit(): void {
        this.Esp8266SubscribeToTopic();
    }


    ngOnDestroy() : void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.subscription2) {
            this.subscription2.unsubscribe();
        }
        if (this.subscription3) {
            this.subscription3.unsubscribe();
        }

    }

    

    private Esp8266SubscribeToTopic() {
        console.log("Start trying to subscribe to broker")
        this.subscription = this._esp8266service.topic(this.sensorID,this.eventNameTemp)
        .subscribe( 
            (data: IMqttMessage) => {
                let item = JSON.parse(data.payload.toString());
                //this.esp8266_events.push(item);
                //this.esp8266_events_temperature = item;
                this.now = new Date();
                this.esp8266_events_temperature.push({'time': this.now.toLocaleString(), 'value':item});
                if (this.esp8266_events_selected['type'] == 'Temperature') {this.pushData(this.esp8266_events_temperature)}
            }
            )

        this.subscription2 = this._esp8266service.topic(this.sensorID,this.eventNamePress)
        .subscribe( 
            (data: IMqttMessage) => {
                let item = JSON.parse(data.payload.toString());
                //this.esp8266_events.push(item);
                //this.esp8266_events_pressure = item;
                this.now = new Date();
                this.esp8266_events_pressure.push({'time': this.now.toLocaleString(), 'value':item});
                if (this.esp8266_events_selected['type'] == 'Pressure') {this.pushData(this.esp8266_events_pressure)}
                
            }
            )

        this.subscription3 = this._esp8266service.topic(this.sensorID,this.eventNameHumidity)
        .subscribe( 
            (data: IMqttMessage) => {
                let item = JSON.parse(data.payload.toString());
                //this.esp8266_events.push(item);
                //this.esp8266_events_humidity = item;
                this.now = new Date();

                this.esp8266_events_humidity.push({'time': this.now.toLocaleString(), 'value':item});
                if (this.esp8266_events_selected['type'] == 'Humidity') {this.pushData(this.esp8266_events_humidity)}
                
            }
            )
        
    }

    public selectHistorygram(type : string) {
        if (type == 'Temperature' || type == 'Humidity' || type == 'Pressure') {
            this.esp8266_events_selected['type'] = type;
            if (this.esp8266_events_selected['type'] == 'Humidity') {this.pushData(this.esp8266_events_humidity)}
            if (this.esp8266_events_selected['type'] == 'Pressure') {this.pushData(this.esp8266_events_pressure)}
            if (this.esp8266_events_selected['type'] == 'Temperature') {this.pushData(this.esp8266_events_temperature)}

        }
    }

    private pushData(data : any[]) {
        this.esp8266_events_selected['events'] = data;
    }
}
