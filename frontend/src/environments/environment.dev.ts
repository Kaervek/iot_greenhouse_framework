export const environment = {
    production: false,
    hmr: false,
    http: {
        apiUrl: '192.168.25.100',// ADJUST TO YOUR SETTINGS
    },
    mqtt: {
        server: '192.168.25.100',// ADJUST TO YOUR SETTINGS
        protocol: "ws",
        port: 8883,
        clientId: 'mqttx_Angular'
    }
  };