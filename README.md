# IoT_Greenhouse_Framework

## Description

This project is intended to develop a web based application that allows via MQTT transmit messages to control and monitor an environment of a greenhouse. The project includes a range of APIs to communicate with a variaty of sensors/actuators.

## Getting started

## Installation

This project is a development environment to create an IoT Greenhouse Framework that can be ported with Docker Containers to Windows/Linux/MAC OS.
Of course it could have limitation on functionalities as mention on chapter Bugs and Limitations.

Required Tools:
- Docker
- Nodejs LTS (includes npm)
- MongoDB
- Make > https://www.gnu.org/software/make/
- Visual studio code or other IDE

## Usage

The repo is divided on several main chapters/subfolders:

### Frontend

As the  name refers it is the user interface. Where the beauty of the logic happens and the user can interact and configure the application.

To work with it, please ensure first to have the node_modules folder created or updated.

```
cd iot_greenhouse_framework
cd frontend
npm install
```
### Backend - Server side

Server side to deploy CRUD services with the databases, handle MQTT and middleware functionalties.

```
cd iot_greenhouse_framework
cd server
npm install
```

### Simulation Server

Simulation server to estimulate and create behaviors without the HW, towards the server and backend.

```
cd iot_greenhouse_framework
cd SimulationServer
npm install
```

### OnField

This folder will containt the HW examples and development of different modules.

Currently Featuring :

- ESP8266
    - BME280 sensor (humidity,temperature,pressure)
    - MQTT communication

### How to run it?

As soon as the node_modules have been installed.

1. We need to generate the docker images for database/frontend/backend/SimulationServer

In order to do this we will use the Makefile and call run on the same folder with a terminal.

```
make build-all
```

This will take a while, it is calling to create docker images, at the end you should have 4 docker images:
- frontend-image-${N}:${N}
- server-image-${N}:${N}
- simulation-server-image-${N}:${N}
- mongodb-server-image-${N}:${N}

The${N} will be the target version of the image

2. Review the following points to change on the code (this steps shall be improved on the future)

In frontend/src/environments/environment.dev.ts adjust the IPs to the one is going to run the simulation. >> 192.168.xx.xx or whatever you have in your local network.

Next only specific to HW:

On OnField/ESP8266/src/sdkconfig is important to modify  the following parameters:

CONFIG_BROKER_URI  >> again tothe proper IP  of the server
main/Kconfig.projbuild >> againg  the  IP >> 192.168.xx.xx to connect with the  server

There are  other IPs but as the  docker compose is set  in order to be launched on the same PC environment and docker compose creates one network, localhost works fine. For a production environment those shall be changed.

3. Execute `make run-all` or `make run-real` based if you want to have a SimulationServer running behind or not.

This will launch a fronted app at  localhost:4200 with 2 pages login (does nothing) and home (small dashboard with examples of lectures in gauges)

## Roadmap & Future IDEAS

- Include water pump actuator and control via app (SW and HW ESP8266)
- Include relay controller (SW and HW ESP8266)
- Include Authentication & Users structure to be Stored on Database
- Panel Configuration (how to add and remove sensors/actuators) based on current devices

## License
As an open source project it  applies the GNU General Public License (GPL)

## Project status

UNDER DEVELOPMENT

##  BUGS AND LIMITATIONS

### Docker
1. Windows Docker Container for ESP8266 lacks functionality to provide access to the USB. Attempts to use WSL allowed to link the usb device but not into the container to bypass it.
### Frontend
1. Current simulation on frontend app the historygram component works on linux but not in windows web browsers as in line 173 this.series.data is unitialized.
2. Error on multiple connections, fronted requires a factory in order to avoid duplication of users that will impact that data will be received only by one when you have the same page from two tabs of a web browser.