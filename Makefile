SERVER_PATH:=./server
SIMULATION_SERVER_PATH:=./SimulationServer
FRONTEND_PATH:=./frontend
DATABASE_PATH:=./database

N?=4

.PHONY: run-all
run-all:
	docker compose -f ./docker-compose.yml up

.PHONY: build-frontend
build-frontend:
	docker buildx b ${FRONTEND_PATH} --file ${FRONTEND_PATH}/Dockerfile-frontend -t frontend-image-${N}:${N}

.PHONY: build-server
build-server:
	docker buildx b ${SERVER_PATH} --file ${SERVER_PATH}/Dockerfile-server -t server-image-${N}:${N}

.PHONY: build-simulation-server
build-simulation-server:
	docker buildx b ${SIMULATION_SERVER_PATH} --file ${SIMULATION_SERVER_PATH}/Dockerfile-simulation-server -t simulation-server-image-${N}:${N}

.PHONY: build-mongodb
build-mongodb:
	docker buildx b ./database --file ./database/Dockerfile-db -t mongodb-server-image-${N}:${N}

.PHONY: run-server
run-server:
	docker run -it -p 3000:3000 -p 1883:1883 -p 8883:8883 server-image-${N}:${N}

.PHONY: run-simulation-server
run-simulation-server:
	docker run -it simulation-server-image-${N}:${N}

.PHONY: build-all
build-all:
	docker buildx b ${FRONTEND_PATH} --file ${FRONTEND_PATH}/Dockerfile-frontend -t frontend-image-${N}:${N}
	docker buildx b ${SERVER_PATH} --file ${SERVER_PATH}/Dockerfile-server -t server-image-${N}:${N}
	docker buildx b ${SIMULATION_SERVER_PATH} --file ${SIMULATION_SERVER_PATH}/Dockerfile-simulation-server -t simulation-server-image-${N}:${N}
	docker buildx b ${DATABASE_PATH} --file ${DATABASE_PATH}/Dockerfile-db -t mongodb-server-image-${N}:${N}


.PHONY: run-real
run-real:
	docker compose -f ./docker-compose_real.yml up
